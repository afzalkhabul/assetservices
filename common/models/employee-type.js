var server = require('../../server/server');
module.exports = function(Employeetype) {
  Employeetype.validatesUniquenessOf('name', {message: 'Name already existed'});

  Employeetype.observe('before save', function (ctx, next) {
    if (ctx.instance != undefined && ctx.instance != null) {
      ctx.instance.name=(ctx.instance.name.toLowerCase());
      ctx.instance.createdTime = new Date();
      next();
    } else {
      if(ctx.data.name!=undefined && ctx.data.name!=null && ctx.data.name!=''){
        ctx.data.name=(ctx.data.name.toLowerCase());
      }
      ctx.data.updatedTime = new Date();
      next();
    }

  });


  Employeetype.remoteMethod('importEmployeeData', {
    description: "Send Valid Details",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'someData', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/importEmployeeData',
      verb: 'POST'
    }
  });

  Employeetype.importEmployeeData = function (data, cb) {
    var url=data[0].id
    var fs=require('fs');
    var request = require('request');
    var result= request('http://localhost:8866/api/Uploads/dhanbadDb/download/'+url).pipe(
      fs.createWriteStream('employeeDetails.csv'));
    result.on('finish', function (err,data) {
      var path = require("path");
      var Converter = require("csvtojson").Converter;
      var converter = new Converter({});
      converter.on("end_parsed", function (jsonArray) {
        if(jsonArray!=null && jsonArray.length>0){
          var failureDetails=[];
          var successfulDetails=[];
          var countDetails=0;
          for(var i=0;i<jsonArray.length;i++){
            var employeeDetails=jsonArray[i];
            if(employeeDetails!=undefined && employeeDetails!=null && employeeDetails!=''){
              var failureStatus=false;
              if(employeeDetails.EmployeeId==undefined || employeeDetails.EmployeeId==null || employeeDetails.EmployeeId==''){
                var message='Please Enter Valid Employee Id';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.EmployeeEmail==undefined || employeeDetails.EmployeeEmail==null || employeeDetails.EmployeeEmail==''){
                var message='Please Enter Valid Employee Email Id';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.EmployeeDisplayName==undefined || employeeDetails.EmployeeDisplayName==null || employeeDetails.EmployeeDisplayName==''){
                var message='Please Enter Valid Employee Name';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.Password==undefined || employeeDetails.Password==null || employeeDetails.Password==''){
                var message='Please Enter Valid Password';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(!failureStatus){
               var Employee=server.models.Employee;
                var employeeInfo=employeeDetails;
                var employeeObject={
                  "firstName": employeeInfo.EmployeeFirstName,
                  "employeeId": employeeInfo.EmployeeId,
                  "lastName": employeeInfo.EmployeeLastName,
                  "email": employeeInfo.EmployeeEmail,
                  "status": "Active",
                  "name": employeeInfo.EmployeeDisplayName,
                  "designation": employeeInfo.EmployeeDesigantion,
                  "department": employeeInfo.EmployeeDepartment,
                  "employeeType": employeeInfo.EmployeeType,
                  "mobile": employeeInfo.PersonalNumber,
                  "workMobile": employeeInfo.WorkNumber,
                  "confirmPassword": employeeInfo.ConfirmPassword,
                  "password":employeeInfo.Password,
                  "role": "employee"
                }
                Employee.create(employeeObject,function(err, createdData){
                  if(err){
                    var message='This Email Id or Employee Id already existed';
                    employeeInfo['errorMessage']=message;
                    failureStatus=true;
                    failureDetails.push(employeeInfo);
                    countDetails++;
                    if(countDetails==jsonArray.length){
                      cb(null, failureDetails)
                    }
                  }else{
                    employeeInfo['errorMessage']='success';
                    failureDetails.push(employeeInfo);
                    countDetails++;
                    if(countDetails==jsonArray.length){
                      cb(null, failureDetails)
                    }
                  }
                });
                }else{
                countDetails++;
                if(countDetails==jsonArray.length){
                  cb(null, failureDetails)
                }
              }
            }else{
              countDetails++;
              if(countDetails==jsonArray.length){
                cb(null, failureDetails)
              }
            }
          }

        }
      });
      require("fs").createReadStream(path.join('employeeDetails.csv')).pipe(converter);
    })
  };

};
