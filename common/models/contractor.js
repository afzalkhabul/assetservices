  var server = require('../../server/server');
  module.exports = function(Contractor) {
    Contractor.validatesUniquenessOf('email', {message: 'Email could be unique'});
    Contractor.observe('before save', function (ctx, next) {
      if(ctx.data!=undefined && ctx.data!=null){
        ctx.data['updatedTime']=new Date();
        var Sms = server.models.Sms;
        next();
      }
      else {
        checkUniqueId(ctx,next);
        ctx.instance.createdTime=new Date();
      }
    });
    function checkUniqueId(ctx,next){
      var randomstring = require("randomstring");
      var uniqueId = randomstring.generate({
        length: 7,
        charset: 'alphanumeric'
      });
      var date=new Date();
      var month;
      var dateIS;
      if((date.getMonth()+1)<10){
        month='0'+(date.getMonth()+1);
      }else{
        month=(date.getMonth()+1);
      }
      if(date.getDate()<10){
        dateIS='0'+date.getDate();
      }else{
        dateIS=date.getDate();
      }
      var string=date.getFullYear()+''+month+dateIS;
      console.log('date is '+date.getFullYear()+'data'+string);
      uniqueId=string+uniqueId;

      var Employee = server.models.Employee;
      Employee.find({'where':{'email':ctx.instance.email}}, function (err, employeeDetails) {
        if(employeeDetails!=undefined && employeeDetails!=null && employeeDetails.length>0){
          next();
        }else{
          Contractor.find({"where": {"employeeId": uniqueId}}, function (err, customers) {
            if(customers.length==0){
              ctx.instance.employeeId = uniqueId;
              var WorkflowForm=server.models.WorkflowForm;
              var Workflow=server.models.Workflow;
              var WorkflowEmployees=server.models.WorkflowEmployees;
              WorkflowForm.findOne({"where": {"schemeUniqueId": "contractor"}}, function (err, workflowForm) {
                if(workflowForm!=undefined && workflowForm!=null ){
                  Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
                     WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
                      var workflowData = [];
                      if (listData != undefined && listData != null && listData.length > 0) {
                        for (var i = 0; i < listData.length; i++) {
                          var flowData = {
                            workflowId: listData[i].workflowId,
                            levelId: listData[i].levelId,
                            status: listData[i].status,
                            maxLevels: listData[i].maxLevels,
                            levelNo: listData[i].levelNo
                          }
                          workflowData.push(flowData);
                        }
                      }
                      ctx.instance.workflow = workflowData;
                      ctx.instance.acceptLevel = 0;
                      ctx.instance.finalStatus = false;
                      ctx.instance.workflowId = workflowForm.workflowId;
                      ctx.instance.maxlevel = flowDataList.maxLevel;
                      ctx.instance.createdTime = new Date();
                      next();
                    });

                  });
                }
              });
            }else{
              checkUniqueId(ctx,next);
            }
          });

        }
      });

    }


    Contractor.observe('loaded', function (ctx, next) {
      if(ctx.instance){
        if(ctx.instance.workflowId!=undefined && ctx.instance.workflowId!=null && ctx.instance.workflowId!='' ){
          var WorkflowEmployees=server.models.WorkflowEmployees;
          WorkflowEmployees.find({'where':{'and':[{'workflowId':ctx.instance.workflowId},{"status": "Active"}]}}, function (err, workflowEmployeeList) {
            if(workflowEmployeeList!=null && workflowEmployeeList.length>0){
              ctx.instance.workflowData=workflowEmployeeList;
              next();
            }else{
              next();
            }
          })
        }else{
          next();
        }
      }else{
        next();
      }
    });



    Contractor.getDetails = function (employee, cb) {
       Contractor.find({'where':{'finalStatus':false}},function (err, requestList) {
        var listRequest=[];
        if(requestList!=undefined && requestList!=null && requestList.length>0){


          var Employee=server.models.Employee;
          var adminStatus=false;
          if(employee.employeeId!=undefined && employee.employeeId!=null){
            Employee.find({'where':{"employeeId":employee.employeeId}}, function (err, employeeList) {
              if(employeeList!=null && employeeList.length>0){
                var firstEmployeeDetails=employeeList[0];
                if(firstEmployeeDetails.role=='superAdmin' || firstEmployeeDetails.role=='schemeAdmin' || firstEmployeeDetails.role=='landAdmin'
                  ||  firstEmployeeDetails.role=='legalAdmin' || firstEmployeeDetails.role=='projectAdmin' ){
                  adminStatus=true;
                }
                for(var i=0;i<requestList.length;i++){
                  var data=requestList[i];
                  var workflowDetails=data.workflowData;
                  var approveFiledVisit=false;
                  var acceptLevel=data.acceptLevel;

                  if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
                    for(var j=0;j<workflowDetails.length;j++){
                      var accessFiledVisit=false;
                      if(workflowDetails[j].employees!=undefined && workflowDetails[j].employees!=null && workflowDetails[j].employees.length>0 ){
                        var employeeList=workflowDetails[j].employees;
                        if(employeeList!=undefined && employeeList!=null && employeeList.length>0 ){
                          for(var x=0;x<employeeList.length;x++){
                            if(employeeList[x]==employee.employeeId){
                              accessFiledVisit=true;
                              break;
                            }
                          }
                        }
                        if(accessFiledVisit || adminStatus){
                          var workFlowLevel=parseInt(workflowDetails[j].levelNo);
                          if(acceptLevel==(workFlowLevel-1)){
                            approveFiledVisit=true;
                            break;
                          }
                        }
                      }
                    }
                  }
                  if(accessFiledVisit || adminStatus){
                    data.editStatus=approveFiledVisit;
                    if(accessFiledVisit){
                      data.availableStatus=accessFiledVisit;
                    }else if(adminStatus){
                      data.availableStatus=adminStatus;
                    }
                    listRequest.push(data);
                  }
                }
                cb(null,listRequest);

              }else{
                cb(null,listRequest);
              }
            });

          }else{
            cb(null,listRequest);
          }
        }else{
          cb(null,listRequest);
        }
      });
    };

    Contractor.remoteMethod('getDetails', {
      description: "Send Valid EmployeeId",
      returns: {
        arg: 'data',
        type: "object",
        root:true
      },
      accepts: [{arg: 'employeeId', type: 'object', http: {source: 'query'}}],
      http: {
        path: '/getDetails',
        verb: 'GET'
      }
    });


    Contractor.updateDetails = function (request, cb) {
      Contractor.findOne({'where':{'id':request.requestId}},function (err, requestDetails) {
        if(requestDetails!=undefined  && requestDetails!=null){

          if(request.acceptLevel!=null){
            var acceptLevelStatus=false;
            var acceptLevel=request.acceptLevel;
            var rejectStatus=false;
            var finalApproval=false;
            var workflowList=[];
            var workflowData=requestDetails.workflow;
            if(workflowData!=undefined && workflowData!=null && workflowData.length>0){
              for(var i=0;i<workflowData.length;i++){
                var levelNo=parseInt(workflowData[i].levelNo);
                var maximumLevel=parseInt(workflowData[i].maxLevels);
                if(levelNo==(acceptLevel+1)){
                  console.log('both are same');
                  acceptLevelStatus=true;
                  if((acceptLevel+1)==maximumLevel){
                    finalApproval=true;
                  }
                  if(request.acceptStatus=='Yes'){
                    workflowData[i].acceptStatus=request.acceptStatus;
                    workflowData[i].requestStatus=request.requestStatus;
                    workflowData[i].comment=request.comment;
                    workflowData[i].employeeId=request.employeeId;
                    workflowData[i].employeeName=request.employeeName;
                    workflowList.push(workflowData[i]);
                  }else{
                    rejectStatus=true;
                    workflowData[i].acceptStatus=request.acceptStatus;
                    workflowData[i].requestStatus=request.requestStatus;
                    workflowData[i].comment=request.comment;
                    workflowData[i].employeeId=request.employeeId;
                    workflowData[i].employeeName=request.employeeName;
                    workflowList.push(workflowData[i]);
                  }


                }else{
                  workflowList.push(workflowData[i]);
                }
              }

            }
            if(acceptLevelStatus==true){
              var updatedData={};
              if(rejectStatus){
                updatedData.acceptLevel=request.acceptLevel+1;
                updatedData.workflow=workflowList;
                updatedData.finalStatus='Rejected';
                updatedData.requestStatus='Rejected';
                updatedData.acceptStatus='No';
                updatedData.comment=request.comment;
              }else{
                updatedData.acceptLevel=request.acceptLevel+1;
                updatedData.workflow=workflowList;
                if(finalApproval){
                  updatedData.finalStatus='Approved';
                  updatedData.requestStatus='Approval';
                  updatedData.acceptStatus='Yes';
                  updatedData.comment=request.comment;
                }
              }
              requestDetails.updateAttributes(updatedData,function (err, updatedDetails) {
                  var Sms = server.models.Sms;
                if (updatedDetails.finalStatus != undefined && updatedDetails.finalStatus != "Approved" && updatedDetails.finalStatus != "Rejected") {
                  var Emailtemplete = server.models.EmailTemplete;

                } else if (updatedDetails.finalStatus != undefined && updatedDetails.finalStatus == "Approved") {
                    var randomstring = require("randomstring");
                    var password = randomstring.generate({
                      length: 7,
                      charset: 'alphanumeric'
                    });
                    var detailsForCreate={
                      'email':updatedDetails.email,
                      'name':updatedDetails.name,
                      'password':password,
                      'employeeId':updatedDetails.email,
                      'department':'consultant',
                      'confirmPassword':password
                    }
                  var Employee = server.models.Employee;
                  Employee.create(detailsForCreate,function(err, employeeDetails){
                    updatedDetails=employeeDetails;
                    var Emailtemplete = server.models.EmailTemplete;
                    Emailtemplete.find({},function (err, emailTemplete) {
                      var Dhanbademail = server.models.DhanbadEmail;
                      Dhanbademail.create({
                        "to": updatedDetails.email,
                        "subject": "Contract Approved",
                        "text": "Use the Provided User Name and Password to Login in the Portal." +"Your Email Id : "+ updatedDetails.email + "  and Password :"+detailsForCreate.password
                      }, function (err, emailId) {

                      });
                    });
                  });

                } else if (updatedDetails.finalStatus != undefined && updatedDetails.finalStatus == "Rejected") {
                 /* console.log("rejected level entered ******************");
                  var Emailtemplete = server.models.EmailTemplete;
                  Emailtemplete.find({},function (err, emailTemplete) {
                    console.log("emailTemplete"+JSON.stringify(emailTemplete));
                    var Dhanbademail = server.models.DhanbadEmail;

                    Dhanbademail.create({
                      "to": updatedDetails.emailId,

                      "subject": emailTemplete[0].rejectedEmail,
                      "text": emailTemplete[0].rejectedMessage

                    }, function (err, emailId) {
                      console.log(emailId);
                    });
                    if(emailTemplete[0].rejectedSMS && updatedDetails.mobileNumber) {
                      var smsData = {
                        "message": emailTemplete[0].rejectedSMS,
                        "mobileNo": updatedDetails.mobileNumber,
                        "smsservicetype": "singlemsg"
                      };

                      Sms.create(smsData, function (err, smsInfo) {
                        console.log('SMS info:' + JSON.stringify(smsInfo));
                      });
                    }
                    //next();
                  });*/
                }
                cb(null,updatedDetails);
              })
            }
          }
        }
      })
    };

    Contractor.remoteMethod('updateDetails', {
      description: "Send Valid EmployeeId",
      returns: {
        arg: 'data',
        type: "object"
      },
      accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
      http: {
        path: '/updateDetails',
        verb: 'POST'
      }
    });

    //After Save Method
    Contractor.observe('after save', function (ctx, next) {
      var Request = server.models.Request;
      if(ctx.isNewInstance){
/*

        var Emailtemplete = server.models.EmailTemplete;
        Emailtemplete.find({},function (err, emailTemplete) {
          console.log("emailTemplete"+JSON.stringify(emailTemplete));
          var Dhanbademail = server.models.DhanbadEmail;

          Dhanbademail.create({
            "to": ctx.instance.emailId,

            "subject": emailTemplete[0].requestEmail,
            "text": emailTemplete[0].requestText

          }, function (err, emailId) {
            console.log(emailId);
          });
          if(emailTemplete[0].registerSMS && ctx.instance.mobileNumber) {
            var Sms = server.models.Sms;
            var smsData = {
              "message": emailTemplete[0].registerSMS,
              "mobileNo": ctx.instance.mobileNumber,
              "smsservicetype": "singlemsg"
            };

            Sms.create(smsData, function (err, smsInfo) {
              console.log('SMS info:' + JSON.stringify(smsInfo));
            });
          }
          next();
        });

*/
        next();

      } else {
        next();
      }
    });

  };
