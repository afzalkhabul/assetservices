  var server = require('../../server/server');
  module.exports = function(Demandprojection) {


    Demandprojection.observe('before save', function (ctx, next) {
      if(ctx.data!=undefined && ctx.data!=null){
        ctx.data['updatedTime']=new Date();
        next();
      }
      else {
        checkUniqueId(ctx,next);
        ctx.instance.createdTime=new Date();

      }
    });
    function checkUniqueId(ctx,next){
      var randomstring = require("randomstring");
      var uniqueId = randomstring.generate({
        length: 7,
        charset: 'alphanumeric'
      });
      var date=new Date();
      var month;
      var dateIS;
      if((date.getMonth()+1)<10){
        month='0'+(date.getMonth()+1);
      }else{
        month=(date.getMonth()+1);
      }
      if(date.getDate()<10){
        dateIS='0'+date.getDate();
      }else{
        dateIS=date.getDate();
      }
      var string=date.getFullYear()+''+month+dateIS;
      uniqueId=string+uniqueId;
      Demandprojection.find({"where": {"requestId": uniqueId}}, function (err, customers) {
        if(customers.length==0){
          ctx.instance.requestId = uniqueId;
          var WorkflowForm=server.models.WorkflowForm;
          var Workflow=server.models.Workflow;
          var WorkflowEmployees=server.models.WorkflowEmployees;
          WorkflowForm.findOne({"where": {"schemeUniqueId": "demandProjection"}}, function (err, workflowForm) {
            if(workflowForm!=undefined && workflowForm!=null ){
              Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
                WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
                  var workflowData = [];
                  if (listData != undefined && listData != null && listData.length > 0) {
                    for (var i = 0; i < listData.length; i++) {
                      var flowData = {
                        workflowId: listData[i].workflowId,
                        levelId: listData[i].levelId,
                        status: listData[i].status,
                        maxLevels: listData[i].maxLevels,
                        levelNo: listData[i].levelNo
                      }
                      workflowData.push(flowData);
                    }
                  }
                  ctx.instance.workflow = workflowData;
                  ctx.instance.acceptLevel = 0;
                  ctx.instance.finalStatus = false;
                  ctx.instance.workflowId = workflowForm.workflowId;
                  ctx.instance.maxlevel = flowDataList.maxLevel;
                  ctx.instance.createdTime = new Date();
                  next();
                });

              });
            }else{
              next();
            }
          });
        }else{
          checkUniqueId(ctx,next);
        }
      });
    }


    Demandprojection.observe('loaded', function (ctx, next) {
      if(ctx.instance){
        if(ctx.instance.workflowId!=undefined && ctx.instance.workflowId!=null && ctx.instance.workflowId!='' ){
          var WorkflowEmployees=server.models.WorkflowEmployees;
          WorkflowEmployees.find({'where':{'and':[{'workflowId':ctx.instance.workflowId},{"status": "Active"}]}}, function (err, workflowEmployeeList) {
            if(workflowEmployeeList!=null && workflowEmployeeList.length>0){
              ctx.instance.workflowData=workflowEmployeeList;
              next();
            }else{
              next();
            }
          })
        }else{
          next();
        }
      }else{
        next();
      }
    });

    Demandprojection.getDetails = function (employee, cb) {
      Demandprojection.find({'where':{'and':[{'department':employee.department},{'financialYear':employee.financialYear}]}},function (err, billDetails) {
        var listRequest=[];
        var requestList=[]
        if(billDetails!=undefined && billDetails!=null && billDetails.length>0){
          for(var bill=0;bill<billDetails.length;bill++){
            var details=billDetails[bill];
            if(details.finalStatus){
              details.editStatus=false;
              details.availableStatus=false;
              details.approveFiledVisit=false;
              listRequest.push(details);
            }else{
              requestList.push(details);
            }
          }
        }

        if(requestList!=null && requestList.length>0){

          var Employee=server.models.Employee;
          var adminStatus=false;
          Employee.find({'where':{"employeeId":employee.employeeId}}, function (err, employeeList) {
            if(employeeList!=null && employeeList.length>0) {
              var firstEmployeeDetails = employeeList[0];
              if (firstEmployeeDetails.role == 'superAdmin' || firstEmployeeDetails.role == 'landAdmin') {
                adminStatus = true;
              }
              for (var i = 0; i < requestList.length; i++) {
                var data = requestList[i];
                var workflowDetails = data.workflowData;
                var accessFiledVisit = false;
                var approveFiledVisit = false;
                if (workflowDetails != undefined && workflowDetails != null && workflowDetails.length > 0) {
                  var acceptLevel = data.acceptLevel;
                  var editDetails;
                  if (data.createdPersonId == employee.employeeId) {
                    editDetails = true;
                  }
                  var availableStatus = false;
                  if (data.finalStatus == false) {
                    for (var j = 0; j < workflowDetails.length; j++) {
                      var availableStatusLevel = false;
                      if (workflowDetails[j].employees != undefined && workflowDetails[j].employees != null && workflowDetails[j].employees.length > 0) {
                        var employeeList = workflowDetails[j].employees;
                        if (employeeList != undefined && employeeList != null && employeeList.length > 0) {
                          for (var x = 0; x < employeeList.length; x++) {
                            if (employeeList[x] == employee.employeeId) {
                              availableStatus = true;
                              availableStatusLevel = true;
                              break;
                            }
                          }
                        }
                        if (availableStatusLevel) {
                          var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                          console.log('workflow level' + workFlowLevel + " accepct level" + acceptLevel);
                          if (acceptLevel == (workFlowLevel - 1)) {
                            approveFiledVisit = true;
                          }
                        }else if(adminStatus){
                          availableStatus=true;
                          var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                          console.log('workflow level' + workFlowLevel + " accepct level" + acceptLevel);
                          if (acceptLevel == (workFlowLevel - 1)) {
                            approveFiledVisit = true;
                          }
                        }

                      }

                    }
                    if (availableStatus || editDetails ) {
                      if (editDetails) {
                        if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                          data.editDetails = true;
                        } else {
                          data.editDetails = false;
                        }
                      }
                      data.approveFiledVisit = approveFiledVisit;
                      data.availableStatus = availableStatus;
                      listRequest.push(data);
                    }
                  }else{
                    if (editDetails) {
                      if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                        data.editDetails = true;
                      } else {
                        data.editDetails = false;
                      }
                    }
                    data.approveFiledVisit = false;
                    data.availableStatus = true;
                    listRequest.push(data);
                  }


                }
              }
              cb(null,listRequest);
            }else{
              cb(null,listRequest);
            }
          });
        }
        else{
          cb(null,listRequest);
        }
      });
    };

    Demandprojection.remoteMethod('getDetails', {
      description: "Send Valid EmployeeId",
      returns: {
        arg: 'data',
        type: "object",
        root:true
      },
      accepts: [{arg: 'employeeId', type: 'object', http: {source: 'query'}}],
      http: {
        path: '/getDetails',
        verb: 'GET'
      }
    });


    Demandprojection.updateDetails = function (request, cb) {
      Demandprojection.findOne({'where':{'id':request.requestId}},function (err, requestDetails) {
        if(requestDetails!=undefined  && requestDetails!=null){
          if(request.acceptLevel!=null){
            var acceptLevelStatus=false;
            var acceptLevel=request.acceptLevel;
            var rejectStatus=false;
            var finalApproval=false;
            var workflowList=[];
            var workflowData=requestDetails.workflow;
            if(workflowData!=undefined && workflowData!=null && workflowData.length>0){
              for(var i=0;i<workflowData.length;i++){
                var levelNo=parseInt(workflowData[i].levelNo);
                var maximumLevel=parseInt(workflowData[i].maxLevels);
                if(levelNo==(acceptLevel+1)){
                  console.log('both are same');
                  acceptLevelStatus=true;
                  if((acceptLevel+1)==maximumLevel){
                    finalApproval=true;
                  }
                  if(request.acceptStatus=='Yes'){
                    workflowData[i].acceptStatus=request.acceptStatus;
                    workflowData[i].requestStatus=request.requestStatus;
                    workflowData[i].comment=request.comment;
                    workflowData[i].employeeId=request.employeeId;
                    workflowData[i].employeeName=request.employeeName;
                    workflowList.push(workflowData[i]);
                  }else{
                    rejectStatus=true;
                    workflowData[i].acceptStatus=request.acceptStatus;
                    workflowData[i].requestStatus=request.requestStatus;
                    workflowData[i].comment=request.comment;
                    workflowData[i].employeeId=request.employeeId;
                    workflowData[i].employeeName=request.employeeName;
                    workflowList.push(workflowData[i]);
                  }


                }else{
                  workflowList.push(workflowData[i]);
                }
              }

            }
            if(acceptLevelStatus==true){
              var updatedData={};
              if(rejectStatus){
                updatedData.acceptLevel=request.acceptLevel+1;
                updatedData.workflow=workflowList;
                updatedData.finalStatus='Rejected';
                updatedData.requestStatus='Rejected';
                updatedData.acceptStatus='No';
                updatedData.comment=request.comment;
              }else{
                updatedData.acceptLevel=request.acceptLevel+1;
                updatedData.workflow=workflowList;
                if(finalApproval){
                  updatedData.finalStatus='Approved';
                  updatedData.requestStatus='Approval';
                  updatedData.acceptStatus='Yes';
                  updatedData.comment=request.comment;
                }
              }
              requestDetails.updateAttributes(updatedData,function (err, updatedDetails) {
                if (updatedDetails.finalStatus != undefined && updatedDetails.finalStatus != "Approved" && updatedDetails.finalStatus != "Rejected") {
                  console.log("request level entered ******************");
               /*   var Emailtemplete = server.models.EmailTemplete;
                  Emailtemplete.find({},function (err, emailTemplete) {
                    console.log("emailTemplete"+JSON.stringify(emailTemplete));
                    var Dhanbademail = server.models.DhanbadEmail;

                    Dhanbademail.create({
                      "to": updatedDetails.emailId,

                      "subject": emailTemplete[0].levelEmail,
                      "text": emailTemplete[0].levelText

                    }, function (err, emailId) {
                      console.log(emailId);
                    });
                    //next();
                  });
*/


                } else if (updatedDetails.finalStatus != undefined && updatedDetails.finalStatus == "Approved") {
                  console.log("Approved level entered ******************");
               /*   var Emailtemplete = server.models.EmailTemplete;
                  Emailtemplete.find({},function (err, emailTemplete) {
                    console.log("emailTemplete"+JSON.stringify(emailTemplete));
                    var Dhanbademail = server.models.DhanbadEmail;

                    Dhanbademail.create({
                      "to": updatedDetails.emailId,

                      "subject": emailTemplete[0].requestApprovalEmail,
                      "text": emailTemplete[0].requestApprovalMessage

                    }, function (err, emailId) {
                      console.log(emailId);
                    });
                    //next();
                  });*/

                } else if (updatedDetails.finalStatus != undefined && updatedDetails.finalStatus == "Rejected") {
                 /* console.log("rejected level entered ******************");
                  var Emailtemplete = server.models.EmailTemplete;
                  Emailtemplete.find({},function (err, emailTemplete) {
                    console.log("emailTemplete"+JSON.stringify(emailTemplete));
                    var Dhanbademail = server.models.DhanbadEmail;

                    Dhanbademail.create({
                      "to": updatedDetails.emailId,

                      "subject": emailTemplete[0].rejectedEmail,
                      "text": emailTemplete[0].rejectedMessage

                    }, function (err, emailId) {
                      console.log(emailId);
                    });
                    //next();
                  });
*/
                }
                cb(null,updatedDetails);
              })

            }
          }
        }
      })
    };

    Demandprojection.remoteMethod('updateDetails', {
      description: "Send Valid EmployeeId",
      returns: {
        arg: 'data',
        type: "object"
      },
      accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
      http: {
        path: '/updateDetails',
        verb: 'POST'
      }
    });


    Demandprojection.updateDetails = function (approvalDetails, cb) {
      Demandprojection.findOne({'where':{'id':approvalDetails.requestId}},function(err, fieldVisitDetails){

        if(fieldVisitDetails!=undefined && fieldVisitDetails!=null){
          var finalObject={};
          var workflowDetails=fieldVisitDetails.workflow;
          if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
            var workflowData=[];
            for(var  i=0;i< workflowDetails.length;i++){
              var data=workflowDetails[i];
              var workFlowLevel=parseInt(workflowDetails[i].levelNo);
              if((approvalDetails.acceptLevel+1)==workFlowLevel){
                data.approvalStatus=approvalDetails.requestStatus;
                data.employeeName=approvalDetails.employeeName;
                data.employeeId=approvalDetails.employeeId;
                data.comment=approvalDetails.comment;
                data.date=new Date();
              }
              workflowData.push(data);
            }
            finalObject.workflow=workflowData;
          }
          if(approvalDetails.requestStatus=="Approval"){
            if(fieldVisitDetails.maxlevel==(approvalDetails.acceptLevel+1)){
              finalObject.finalStatus=approvalDetails.requestStatus;
              finalObject.acceptLevel=approvalDetails.acceptLevel+1;
            }else{
              finalObject.acceptLevel=approvalDetails.acceptLevel+1;
            }
          } else if(approvalDetails.requestStatus=="Rejected"){
            finalObject.finalStatus=approvalDetails.requestStatus;
            finalObject.acceptLevel=approvalDetails.acceptLevel+1;
            finalObject.rejectComment=approvalDetails.comment;
          }
          console.log('before save details'+JSON.stringify(finalObject));
          fieldVisitDetails.updateAttributes(finalObject, function(err, finalData){
            var Sms = server.models.Sms;
            if(finalObject.finalStatus!=undefined && finalObject.finalStatus!=null && finalObject.finalStatus=="Rejected" ){
                  if(finalData.createdPersonId!=undefined && finalData.createdPersonId!=null && finalData.createdPersonId!='' ){
                var Employee=server.models.Employee;
                var EmailTemplete = server.models.EmailTemplete;
                Employee.find({'where':{'employeeId':finalData.createdPersonId}}, function (err, employeeDetails) {
                  if(employeeDetails!=null && employeeDetails.length>0){

                    EmailTemplete.findOne({"where":{"emailType": "landAndAsset"}},function (err, emailTemplete) {
                      if(emailTemplete!=null ) {
                        var nodemailer = require('nodemailer');
                        var smtpTransport = require('nodemailer-smtp-transport');
                        var transporter = nodemailer.createTransport(smtpTransport({
                          host: "smtp.gmail.com",
                          port: 465,
                          secure: true,
                          auth: {
                            user: emailTemplete.emailId,
                            pass: 'admin@admin'
                          }
                        }));
                        var message = emailTemplete.demandProjectionRejectedMessage + '<br>Reason is ' + approvalDetails.comment;
                        var mailOptions = {
                          from: emailTemplete.emailId,
                          to: employeeDetails[0].email,
                          subject: emailTemplete.demandProjectionRejectedEmail, // Subject line
                          text: '', // plaintext body
                          html: message // html body
                        };
                        transporter.sendMail(mailOptions, function (error, info) {
                          if (error) {
                            return console.log(error);
                          }
                        });
                        if(emailTemplete.demandProjectionRejectedSMS && employeeDetails[0].mobile) {
                          var Sms = server.models.Sms;
                          var smsData = {
                            "message": emailTemplete.demandProjectionRejectedSMS,
                            "mobileNo": employeeDetails[0].mobile,
                            "smsservicetype": "singlemsg"
                          };
                          Sms.create(smsData, function (err, smsInfo) {
                          });
                        }
                      }
                    });
                    cb(null, finalData);
                  }else{
                    cb(null, finalData);
                  }
                });

              }else{
                cb(null, finalData);
              }

            }else if(finalObject.finalStatus!=undefined && finalObject.finalStatus!=null && finalObject.finalStatus=="Approval"){
                 if(finalData.createdPersonId!=undefined && finalData.createdPersonId!=null && finalData.createdPersonId!='' ){
                var Employee=server.models.Employee;
                var EmailTemplete = server.models.EmailTemplete;
                Employee.find({'where':{'employeeId':finalData.createdPersonId}}, function (err, employeeDetails) {
                  if(employeeDetails!=null && employeeDetails.length>0){
                    EmailTemplete.findOne({"where":{"emailType": "landAndAsset"}},function (err, emailTemplete) {
                       if(emailTemplete!=null) {
                        var nodemailer = require('nodemailer');
                        var smtpTransport = require('nodemailer-smtp-transport');
                        var transporter = nodemailer.createTransport(smtpTransport({
                          host: "smtp.gmail.com",
                          port: 465,
                          secure: true,
                          auth: {
                            user: emailTemplete.emailId,
                            pass: 'admin@admin'
                          }
                        }));
                        var message = emailTemplete.demandProjectionApprovalMessage + '<br>Reason is ' + approvalDetails.comment;
                        var mailOptions = {
                          from: emailTemplete.emailId,
                          to: employeeDetails[0].email,
                          subject: emailTemplete.demandProjectionApprovalEmail, // Subject line
                          text: '', // plaintext body
                          html: message // html body
                        };
                        transporter.sendMail(mailOptions, function (error, info) {
                          if (error) {
                            return console.log(error);
                          }
                        });
                        if(emailTemplete.demandProjectionApprovalSMS && employeeDetails[0].mobile) {
                          var Sms = server.models.Sms;
                          var smsData = {
                            "message": emailTemplete.demandProjectionApprovalSMS,
                            "mobileNo": employeeDetails[0].mobile,
                            "smsservicetype": "singlemsg"
                          };
                          Sms.create(smsData, function (err, smsInfo) {
                          });
                        }
                      }
                    });
                    cb(null, finalData);
                  }else{
                    cb(null, finalData);
                  }
                });
              }
            }else{
              cb(null, finalData);
            }
          });
        }else {
          var message = 'Please provide valid details';
          cb(message,null);
        }

      });
    };

    Demandprojection.remoteMethod('updateDetails', {
      description: "Send Valid EmployeeId",
      returns: {
        arg: 'data',
        type: "object",
        root:true
      },
      accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
      http: {
        path: '/updateDetails',
        verb: 'POST'
      }
    });

    Demandprojection.updateContent= function (updatedDetails, cb) {
      Demandprojection.findOne({'where':{'id':updatedDetails.id}},function(err, fieldVisitDetails){
        var WorkflowForm=server.models.WorkflowForm;
        var Workflow=server.models.Workflow;
        var WorkflowEmployees=server.models.WorkflowEmployees;
        WorkflowForm.findOne({"where": {"and":[{"status": "Active"},{"schemeUniqueId": "fieldVisit"}]}}, function (err, workflowForm) {
          if(workflowForm!=undefined && workflowForm!=null ){
            Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
              WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
                var workflowData = [];
                if (listData != undefined && listData != null && listData.length > 0) {
                  for (var i = 0; i < listData.length; i++) {
                    var flowData = {
                      workflowId: listData[i].workflowId,
                      levelId: listData[i].levelId,
                      status: listData[i].status,
                      maxLevels: listData[i].maxLevels,
                      levelNo: listData[i].levelNo
                    }
                    workflowData.push(flowData);
                  }
                }
                updatedDetails.workflow = workflowData;
                updatedDetails.workflowId=workflowForm.workflowId;
                updatedDetails.maxlevel=flowDataList.maxLevel;
                updatedDetails.acceptLevel=0;
                updatedDetails.finalStatus=false;
                updatedDetails['updatedTime']=new Date();
                fieldVisitDetails.updateAttributes(updatedDetails,function(err,data){
                });
                cb(null, fieldVisitDetails);
              });

            });
          }
        });




      });
    };

    Demandprojection.remoteMethod('updateContent', {
      description: "Send Valid EmployeeId",
      returns: {
        arg: 'data',
        type: "object",
        root:true
      },
      accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
      http: {
        path: '/updateContent',
        verb: 'POST'
      }
    });


    //After Save Method
    Demandprojection.observe('after save', function (ctx, next) {

      if(ctx.isNewInstance){

      /*  var Emailtemplete = server.models.EmailTemplete;
        Emailtemplete.find({},function (err, emailTemplete) {
          console.log("emailTemplete"+JSON.stringify(emailTemplete));
          var Dhanbademail = server.models.DhanbadEmail;

          Dhanbademail.create({
            "to": ctx.instance.emailId,

            "subject": emailTemplete[0].requestEmail,
            "text": emailTemplete[0].requestText

          }, function (err, emailId) {
            console.log(emailId);
          });
          next();
        });*/
        next();
      } else {
        next();
      }
    });

  };


