var server = require('../../server/server');
module.exports = function(Vehiclepurchaserequest) {

  Vehiclepurchaserequest.observe('before save', function (ctx, next) {

    if(ctx.data!=undefined && ctx.data!=null){
      ctx.data['updatedTime']=new Date();
      next();
    }
    else {
      checkUniqueId(ctx,next);
      ctx.instance.createdTime=new Date();
    }
  });
  function checkUniqueId(ctx,next){
    var randomstring = require("randomstring");
    var uniqueId = randomstring.generate({
      length: 7,
      charset: 'alphanumeric'
    });
    var date=new Date();
    var month;
    var dateIS;
    if((date.getMonth()+1)<10){
      month='0'+(date.getMonth()+1);
    }else{
      month=(date.getMonth()+1);
    }
    if(date.getDate()<10){
      dateIS='0'+date.getDate();
    }else{
      dateIS=date.getDate();
    }
    var string=date.getFullYear()+''+month+dateIS;
    console.log('date is '+date.getFullYear()+'data'+string);
    uniqueId=string+uniqueId;

    Vehiclepurchaserequest.find({"where": {"requestId": uniqueId}}, function (err, customers) {
      if(customers.length==0){
        ctx.instance.requestId = uniqueId;
        var WorkflowForm=server.models.WorkflowForm;
        var Workflow=server.models.Workflow;
        var WorkflowEmployees=server.models.WorkflowEmployees;

        WorkflowForm.findOne({"where": {"schemeUniqueId": 'purchaseAgenda'}}, function (err, workflowForm) {
          if(workflowForm!=undefined && workflowForm!=null ){
            Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
               WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
                var workflowData = [];
                if (listData != undefined && listData != null && listData.length > 0) {
                  for (var i = 0; i < listData.length; i++) {
                    var flowData = {
                      workflowId: listData[i].workflowId,
                      levelId: listData[i].levelId,
                      status: listData[i].status,
                      maxLevels: listData[i].maxLevels,
                      levelNo: listData[i].levelNo
                    }
                    workflowData.push(flowData);
                  }
                }
                ctx.instance.workflow = workflowData;
                ctx.instance.acceptLevel = 0;
                ctx.instance.finalStatus = false;
                ctx.instance.workflowId = workflowForm.workflowId;
                ctx.instance.maxlevel = flowDataList.maxLevel;
                ctx.instance.createdTime = new Date();
                next();
              });

            });
          }else{
            next();
          }
        });


      }else{
        checkUniqueId(ctx,next);
      }
    });
  }


  Vehiclepurchaserequest.observe('loaded', function (ctx, next) {
    if(ctx.instance){
      if(ctx.instance.workflowId!=undefined && ctx.instance.workflowId!=null && ctx.instance.workflowId!='' ){
        var WorkflowEmployees=server.models.WorkflowEmployees;
        WorkflowEmployees.find({'where':{'and':[{'workflowId':ctx.instance.workflowId},{"status": "Active"}]}}, function (err, workflowEmployeeList) {
          if(workflowEmployeeList!=null && workflowEmployeeList.length>0){
            ctx.instance.workflowData=workflowEmployeeList;
            next();
          }else{
            next();
          }
        })
      }else{
        next();
      }
    }else{
      next();
    }
  });



  Vehiclepurchaserequest.getDetails = function (employee, cb) {
    Vehiclepurchaserequest.find({'where':{'finalStatus':false}},function (err, requestList) {
      var listRequest=[];
      if(requestList!=undefined && requestList!=null && requestList.length>0){
        var Employee=server.models.Employee;
        var adminStatus=false;
        Employee.find({'where':{"employeeId":employee}}, function (err, employeeList) {
          if(employeeList!=null && employeeList.length>0) {
            var firstEmployeeDetails = employeeList[0];
            if (firstEmployeeDetails.role == 'superAdmin' || firstEmployeeDetails.role == 'landAdmin') {
              adminStatus = true;
            }
            for (var i = 0; i < requestList.length; i++) {
              var data = requestList[i];
              var workflowDetails = data.workflowData;
              var approveFiledVisit = false;
              var acceptLevel = data.acceptLevel;
              if (workflowDetails != undefined && workflowDetails != null && workflowDetails.length > 0) {
                for (var j = 0; j < workflowDetails.length; j++) {
                  var accessFiledVisit = false;
                  if (workflowDetails[j].employees != undefined && workflowDetails[j].employees != null && workflowDetails[j].employees.length > 0) {
                    var employeeList = workflowDetails[j].employees;
                    if (employeeList != undefined && employeeList != null && employeeList.length > 0) {
                      for (var x = 0; x < employeeList.length; x++) {
                        if (employeeList[x] == employee) {
                          accessFiledVisit = true;
                          break;
                        }
                      }
                    }
                    if (accessFiledVisit || adminStatus) {
                      var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                      if (acceptLevel == (workFlowLevel - 1)) {
                        approveFiledVisit = true;
                        break;
                      }
                    }
                  }
                }
              }
              if(accessFiledVisit || adminStatus){
                data.editStatus=approveFiledVisit;
                if(accessFiledVisit){
                  data.availableStatus=accessFiledVisit;
                }else if(adminStatus){
                  data.availableStatus=adminStatus;
                }
                listRequest.push(data);
              }
            }
            cb(null,listRequest);
          }
          });
      }else{
        cb(null,listRequest);
      }
    });
  };

  Vehiclepurchaserequest.remoteMethod('getDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'employeeId', type: 'string', http: {source: 'query'}}],
    http: {
      path: '/getDetails',
      verb: 'GET'
    }
  });


  Vehiclepurchaserequest.updateDetails = function (request, cb) {
    Vehiclepurchaserequest.findOne({'where':{'id':request.requestId}},function (err, requestDetails) {
      if(requestDetails!=undefined  && requestDetails!=null){
        if(request.acceptLevel!=null){
          var acceptLevelStatus=false;
          var acceptLevel=request.acceptLevel;
          var rejectStatus=false;
          var finalApproval=false;
          var workflowList=[];
          var workflowData=requestDetails.workflow;
          if(workflowData!=undefined && workflowData!=null && workflowData.length>0){
            for(var i=0;i<workflowData.length;i++){
              var levelNo=parseInt(workflowData[i].levelNo);
              var maximumLevel=parseInt(workflowData[i].maxLevels);
              if(levelNo==(acceptLevel+1)){
                console.log('both are same');
                acceptLevelStatus=true;
                if((acceptLevel+1)==maximumLevel){
                  finalApproval=true;
                }
                if(request.acceptStatus=='Yes'){
                  workflowData[i].acceptStatus=request.acceptStatus;
                  workflowData[i].requestStatus=request.requestStatus;
                  workflowData[i].comment=request.comment;
                  workflowData[i].employeeId=request.employeeId;
                  workflowData[i].employeeName=request.employeeName;
                  workflowList.push(workflowData[i]);
                }else{
                  rejectStatus=true;
                  workflowData[i].acceptStatus=request.acceptStatus;
                  workflowData[i].requestStatus=request.requestStatus;
                  workflowData[i].comment=request.comment;
                  workflowData[i].employeeId=request.employeeId;
                  workflowData[i].employeeName=request.employeeName;
                  workflowList.push(workflowData[i]);
                }
              }else{
                workflowList.push(workflowData[i]);
              }
            }
          }
          if(acceptLevelStatus==true){
            var updatedData={};
            if(rejectStatus){
              updatedData.acceptLevel=request.acceptLevel+1;
              updatedData.workflow=workflowList;
              updatedData.finalStatus='Rejected';
              updatedData.requestStatus='Rejected';
              updatedData.acceptStatus='No';
              updatedData.comment=request.comment;
            }else{
              updatedData.acceptLevel=request.acceptLevel+1;
              updatedData.workflow=workflowList;
              if(finalApproval){
                updatedData.finalStatus='Approved';
                updatedData.requestStatus='Approval';
                updatedData.acceptStatus='Yes';
                updatedData.comment=request.comment;
              }
            }
            requestDetails.updateAttributes(updatedData,function (err, updatedDetails) {
              var Sms = server.models.Sms;
              if (updatedDetails.finalStatus != undefined && updatedDetails.finalStatus != "Approved" && updatedDetails.finalStatus != "Rejected") {
                console.log("request level entered ******************");
                var Emailtemplete = server.models.EmailTemplete;
                Emailtemplete.find({},function (err, emailTemplete) {
                  var Dhanbademail = server.models.DhanbadEmail;
/*
                  Dhanbademail.create({
                    "to": updatedDetails.emailId,

                    "subject": emailTemplete[0].levelEmail,
                    "text": emailTemplete[0].levelText

                  }, function (err, emailId) {
                    console.log(emailId);
                  });*/
                  if(emailTemplete[0].requestApprovalSMS && updatedDetails.mobileNumber) {
                    var smsData = {
                      "message": emailTemplete[0].requestApprovalSMS,
                      "mobileNo": updatedDetails.mobileNumber,
                      "smsservicetype": "singlemsg"
                    };

                    Sms.create(smsData, function (err, smsInfo) {
                      console.log('SMS info:' + JSON.stringify(smsInfo));
                    });
                  }
                });



              } else if (updatedDetails.finalStatus != undefined && updatedDetails.finalStatus == "Approved") {
                var Emailtemplete = server.models.EmailTemplete;
                Emailtemplete.find({},function (err, emailTemplete) {
                  var Dhanbademail = server.models.DhanbadEmail;
                  if(updatedDetails.emailId) {
                    Dhanbademail.create({
                      "to": updatedDetails.emailId,
                      "subject": emailTemplete[0].requestApprovalEmail,
                      "text": emailTemplete[0].requestApprovalMessage

                    }, function (err, emailId) {
                    });
                  }

                  if(emailTemplete[0].requestApprovalSMS && updatedDetails.mobileNumber) {
                    var smsData = {
                      "message": emailTemplete[0].requestApprovalSMS,
                      "mobileNo": updatedDetails.mobileNumber,
                      "smsservicetype": "singlemsg"
                    };
                    Sms.create(smsData, function (err, smsInfo) {
                      console.log('SMS info:' + JSON.stringify(smsInfo));
                    });
                  }
                });

              } else if (updatedDetails.finalStatus != undefined && updatedDetails.finalStatus == "Rejected") {
                console.log("rejected level entered ******************");
                var Emailtemplete = server.models.EmailTemplete;
                Emailtemplete.find({},function (err, emailTemplete) {
                  console.log("emailTemplete"+JSON.stringify(emailTemplete));
                  var Dhanbademail = server.models.DhanbadEmail;

               /*   Dhanbademail.create({
                    "to": updatedDetails.emailId,

                    "subject": emailTemplete[0].rejectedEmail,
                    "text": emailTemplete[0].rejectedMessage

                  }, function (err, emailId) {
                    console.log(emailId);
                  });*/
                  if(emailTemplete[0].requestApprovalSMS && updatedDetails.mobileNumber) {
                    var smsData = {
                      "message": emailTemplete[0].requestApprovalSMS,
                      "mobileNo": updatedDetails.mobileNumber,
                      "smsservicetype": "singlemsg"
                    };
                    Sms.create(smsData, function (err, smsInfo) {

                    });
                  }
                });
              }
              cb(null,updatedDetails);
            })
          }
        }
      }
    });
  };

  Vehiclepurchaserequest.remoteMethod('updateDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object"
    },
    accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateDetails',
      verb: 'POST'
    }
  });

  //After Save Method
  Vehiclepurchaserequest.observe('after save', function (ctx, next) {
    var Vehiclepurchaserequest = server.models.Vehiclepurchaserequest;
    if(ctx.isNewInstance){
      var Sms = server.models.Sms;
      var Emailtemplete = server.models.EmailTemplete;
      Emailtemplete.find({},function (err, emailTemplete) {
        var Dhanbademail = server.models.DhanbadEmail;
        if(ctx.instance.emailId) {
          Dhanbademail.create({
            "to": ctx.instance.emailId,
            "subject": emailTemplete[0].requestEmail,
            "text": emailTemplete[0].requestText
          }, function (err, emailId) {
            console.log(emailId);
          });
        }
        if(emailTemplete[0].requestApprovalSMS && updatedDetails.mobileNumber) {
          var smsData = {
            "message": emailTemplete[0].requestApprovalSMS,
            "mobileNo": updatedDetails.mobileNumber,
            "smsservicetype": "singlemsg"
          };

          Sms.create(smsData, function (err, smsInfo) {
          });
        }
        next();
      });
    } else {
      next();
    }
  });

  /*Get Vehicle Request Data Start*/

   Vehiclepurchaserequest.getVehicleRequestData = function (request, cb) {
   console.log("hello" +JSON.stringify(request))
   console.log("hello request id" +JSON.stringify(request.requestId))
   console.log("hello request status" +JSON.stringify(request.requestStatus))

     if(request!=null && request.requestId!=undefined&& request.requestId!=null && request.requestStatus!=undefined&& request.requestStatus!=null){
       var query;
       var startDate;
       var endDate;
       if(request.duration!=undefined && request.duration!=null){
         var todayDate=new Date();
         console.log("new date" +todayDate)
         if(request.duration=='1'){
           startDate=new Date(new Date() - (7 * 24 * 60 * 60 * 1000));
           console.log("start date for test" +startDate)
           endDate=new Date();
         } else  if(request.duration=='2'){
           startDate=new Date(new Date() - (30 * 24 * 60 * 60 * 1000));
           endDate=new Date();
         }else{
           startDate=new Date(new Date() - (60 * 24 * 60 * 60 * 1000));
           endDate=new Date();
         }

         if(request.requestStatus=='all'){
           query=[{'requestId':request.requestId},{'createdTime': {'gt': startDate}}]
         }else{
           query=[{'requestId':request.requestId},{'createdTime': {'gt': startDate}},{"finalStatus" : request.requestStatus}]
         }
       }
       else if(request.startDate!=undefined && request.startDate!=null && request.endDate!=undefined && request.endDate!=null ){
         startDate=new  Date(request.startDate);
         endDate=new  Date(request.endDate);
         if(request.requestStatus=='all'){
           query=[{'requestId':request.requestId},{'createdTime': {'gt': startDate}},{'createdTime': {'lt': endDate}}]
         }else{
           query=[{'requestId':request.requestId},{'createdTime': {'gt': startDate}},{'createdTime': {'lt': endDate}},{"finalStatus" : request.requestStatus}]
         }
       }
      console.log('query data'+JSON.stringify(query));
      Vehiclepurchaserequest.find({'where':{'and':query}}, function (err, requestList) {
        console.log('length of request'+requestList.length);
        cb(null,requestList);
       });
     } else{

     }

   };

    Vehiclepurchaserequest.remoteMethod('getVehicleRequestData', {
      description: "Send Valid EmployeeId",
      returns: {
        arg: 'data',
        type: "object",
        root:true
      },
      accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
      http: {
        path: '/getVehicleRequestData',
        verb: 'POST'
      }
    });

  /*Get Vehicle Request Data End*/

};

