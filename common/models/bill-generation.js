var server = require('../../server/server');
module.exports = function(Billgeneration) {

  Billgeneration.observe('before save', function (ctx, next) {

    if(ctx.instance){
    checkBillId(ctx,next);
    }else{

      ctx['updatedTime']=new Date();
      next();
    }
  });
  function checkBillId(ctx,next){
    var randomstring = require("randomstring");
    var uniqueId = randomstring.generate({
      length: 7,
      charset: 'alphanumeric'
    });
    var date=new Date();
    var month;
    var dateIS;
    if((date.getMonth()+1)<10)month='0'+(date.getMonth()+1);
    else{
      month=(date.getMonth()+1);
    }
    if(date.getDate()<10){dateIS='0'+date.getDate();}else{dateIS=date.getDate();}
    var string=date.getFullYear()+''+month+dateIS;
    uniqueId=string+uniqueId;
    uniqueId=uniqueId.toUpperCase();
      Billgeneration.find({'where':{'billId':uniqueId}},function(err, billData){
      if(billData && billData.length==0){
        var WorkflowForm=server.models.WorkflowForm;
        var Workflow=server.models.Workflow;
        var WorkflowEmployees=server.models.WorkflowEmployees;
        var notification=server.models.notification;
        ctx.instance.billId=uniqueId;
        WorkflowForm.findOne({"where": {"and":[{"status": "Active"},{"schemeUniqueId": "billGeneration"}]}}, function (err, workflowForm) {
          if(workflowForm!=undefined && workflowForm ){
            Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
              WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
                var workflowData = [];
                var employeeDetailsForNotification=[];
                if (listData != undefined && listData != null && listData.length > 0) {
                  for (var i = 0; i < listData.length; i++) {
                    var flowData = {
                      workflowId: listData[i].workflowId,
                      levelId: listData[i].levelId,
                      status: listData[i].status,
                      maxLevels: listData[i].maxLevels,
                      levelNo: listData[i].levelNo
                    }
                    workflowData.push(flowData);
                      if(listData[i].levelNo=="1" && listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                        for(var j=0;j<listData[i].employees.length;j++){
                          employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                        }
                      }

                  }
                }
                ctx.instance.workflow = workflowData;
                ctx.instance.workflowId=workflowForm.workflowId;
                ctx.instance.maxlevel=flowDataList.maxLevel;
                ctx.instance.acceptLevel=0;
                ctx.instance.finalStatus=false;
                ctx.instance.createdTime=new Date();
                ctx.instance.lastUpdateTime=new Date();

                if(employeeDetailsForNotification.length>0){
                  notification.create({
                    'to':employeeDetailsForNotification,
                    "subject":  'New Tasks',
                    "text":"You got New Plan Bill Request",
                    "message":"You got New Bill Request",
                    'urlData':'billGeneration',
                    "type":"newRequest"
                  });
                }

                var reviewPersonList=[{'employeeId':ctx.instance.reviewPerson,'readStatus':false}];

               if(reviewPersonList.length>0){
                 notification.create({
                   'to':reviewPersonList,
                   "subject":  'New Tasks',
                   "text":"Your Bill Request is submitted successfully",
                   "message":"Your Bill Request is submitted successfully",
                   'urlData':'billGeneration',
                   "type":"newRequest"
                 });
               }
                Billgeneration.find({'where':{'and':[{'planId':ctx.instance.planId},{'finalStatus' : "Approval"}]}}, function (err, result) {
                  if(result && result.length>0){
                    var totalAmount=0;
                    for(var x=0;x<result.length;x++){
                      totalAmount=totalAmount+parseInt(result[x].approvedAmount);
                    }
                    if(totalAmount>0){
                      var ProjectTasks=server.models.ProjectTasks;
                      ProjectTasks.find({'where':{'planId' : ctx.instance.planId}}, function (err, taskList) {
                        if(taskList!=null && taskList.length>0){
                          var estimatedCost=0;
                          for(var i=0;i<taskList.length;i++){
                            var subTaskList=taskList[i].taskList;
                            if(subTaskList!=null && subTaskList.length>0 ){
                              for(var j=0;j<subTaskList.length;j++){
                                if(subTaskList[j].subTaskName==ctx.instance.subtask){
                                  estimatedCost=parseInt(subTaskList[j].estimatedCost);
                                }
                              }
                            }
                          }

                          if(estimatedCost!=0 && estimatedCost>totalAmount){
                            next();
                          }else{
                            notification.create({
                              'to':employeeDetailsForNotification,
                              "subject":  'New Tasks',
                              "text":"You got New Plan Bill Request with over spent amount. Bill Id is "+ctx.instance.billId,
                              "message":"You got New Plan Bill Request with over spent amount. Bill Id is "+ctx.instance.billId,
                              'urlData':'billGeneration',
                              "type":"newRequest"
                            });
                            next();
                            /*  var EmailTemplete = server.models.EmailTemplete;
                             EmailTemplete.findOne({"where":{"emailType": "projectWardWorks"}},function (err, emailTemplete) {
                             if (emailTemplete != null) {

                             var Dhanbademail = server.models.DhanbadEmail;
                             Dhanbademail.create({
                             from: emailTemplete.emailId,
                             to: employeeDetails[0].email,
                             subject: 'You got bill request with over spent amount', // Subject line
                             "text": 'Hi, <br> <p>You got new request for bill approval. This contains overspent amount for this task. Bill id is </p>'+ ctx.instance.billId
                             }, function (err, email) {
                             console.log(email);
                             });
                             }
                             });*/
                          }
                        }else{
                          next();
                        }
                      })
                    }else{
                      next();
                    }
                  }else{
                    next();
                  }
                })
              });
            });
          }
        });
      }else{
        checkBillId();
      }
    });
  }

  Billgeneration.observe('loaded', function (ctx, next) {
    if(ctx.instance){
      if(ctx.instance.reviewPerson!=undefined && ctx.instance.reviewPerson!=null){
        var Employee=server.models.Employee;
        Employee.find({'where':{'employeeId':ctx.instance.reviewPerson}},function(err, employee){
          if(employee.length>0){
            ctx.instance.reviewPersonName=employee[0].name;
          }
        });
      }
      if(ctx.instance.workflowId!=undefined && ctx.instance.workflowId!=null && ctx.instance.workflowId!='' ){
        var WorkflowEmployees=server.models.WorkflowEmployees;
        WorkflowEmployees.find({'where':{'and':[{'workflowId':ctx.instance.workflowId},{"status": "Active"}]}}, function (err, workflowEmployeeList) {
          if(workflowEmployeeList!=null && workflowEmployeeList.length>0){
            ctx.instance.workflowData=workflowEmployeeList;
            next();
          }else{
            next();
          }
        })
      }else{
        next();
      }
    }else{
      next();
    }
  });

  Billgeneration.getDetails = function (employee,planId,subTask,cb) {

    Billgeneration.find({"where":{"and":[{"planId":planId},{"subtask":subTask}]}},function(err, fieldVisitData){
      var fieldVisitList=[];
      if(fieldVisitData!=null && fieldVisitData.length>0){

        var Employee=server.models.Employee;
        var adminStatus=false;
        Employee.find({'where':{"employeeId":employee}}, function (err, employeeList) {
          if(employeeList!=null && employeeList.length>0) {
            var firstEmployeeDetails = employeeList[0];
            if (firstEmployeeDetails.role == 'superAdmin' || firstEmployeeDetails.role == 'projectAdmin') {
              adminStatus = true;
            } else {

            }
            for (var i = 0; i < fieldVisitData.length; i++) {
              var data = fieldVisitData[i];
              var workflowDetails = data.workflowData;
              var accessFiledVisit = false;
              var approveFiledVisit = false;
              if (workflowDetails != undefined && workflowDetails != null && workflowDetails.length > 0) {
                var acceptLevel = data.acceptLevel;
                var editDetails;
                if (data.createdPersonId == employee) {
                  editDetails = true;
                }
                var availableStatus = false;
                if (data.finalStatus == false) {
                  for (var j = 0; j < workflowDetails.length; j++) {
                    var availableStatusLevel = false;
                    if (workflowDetails[j].employees != undefined && workflowDetails[j].employees != null && workflowDetails[j].employees.length > 0) {


                      var employeeList = workflowDetails[j].employees;
                      if (employeeList != undefined && employeeList != null && employeeList.length > 0) {
                        for (var x = 0; x < employeeList.length; x++) {
                          if (employeeList[x] == employee) {
                            availableStatus = true;
                            availableStatusLevel = true;
                            break;
                          }
                        }
                      }
                      if (availableStatusLevel) {
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);

                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                        }
                      }else if(adminStatus){
                        availableStatus=true;
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                        }
                      }

                    }

                  }
                  if (availableStatus || editDetails ) {
                    if (editDetails) {
                      if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                        data.editDetails = true;
                      } else {
                        data.editDetails = false;
                      }
                    }
                    data.approveFiledVisit = approveFiledVisit;
                    data.availableStatus = availableStatus;
                    fieldVisitList.push(data);
                  }
                }else{
                  if (editDetails) {
                    if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                      data.editDetails = true;
                    } else {
                      data.editDetails = false;
                    }
                  }
                  data.approveFiledVisit = false;
                  data.availableStatus = true;
                  fieldVisitList.push(data);
                }
              }
            }
            cb(null,fieldVisitList);
          }else{
            cb(null,fieldVisitList);
          }
        });

      }else{
        cb(null,fieldVisitList);
      }
    });
  };


  Billgeneration.remoteMethod('getDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'employeeId', type: 'string', http: {source: 'query'},required:true},
      {arg: 'planId', type: 'string', http: {source: 'query'},required:true},
      {arg: 'subTask', type: 'string', http: {source: 'query'},required:true}],
    http: {
      path: '/getDetails',
      verb: 'GET'
    }
  });





  Billgeneration.updateDetails = function (approvalDetails, cb) {
    Billgeneration.findOne({'where':{'id':approvalDetails.requestId}},function(err, fieldVisitDetails){
      if(fieldVisitDetails!=undefined && fieldVisitDetails!=null){
        var finalObject={};
        if(fieldVisitDetails.acceptLevel!=undefined && fieldVisitDetails.acceptLevel!=null && fieldVisitDetails.acceptLevel==approvalDetails.acceptLevel){
          var workflowDetails=fieldVisitDetails.workflow;
          if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
            var workflowData=[];
            for(var  i=0;i< workflowDetails.length;i++){
              var data=workflowDetails[i];
              var workFlowLevel=parseInt(workflowDetails[i].levelNo);
              if((approvalDetails.acceptLevel+1)==workFlowLevel){
                data.approvalStatus=approvalDetails.requestStatus;
                data.employeeName=approvalDetails.employeeName;
                data.employeeId=approvalDetails.employeeId;
                data.comment=approvalDetails.comment;
                data.approvedAmount=approvalDetails.approvedAmount;
                data.date=new Date();
              }
              workflowData.push(data);
            }
            finalObject.workflow=workflowData;
            finalObject.lastUpdateTime=new Date();
          }
          if(approvalDetails.requestStatus=="Approval"){
            if(fieldVisitDetails.maxlevel==(approvalDetails.acceptLevel+1)){
              finalObject.finalStatus=approvalDetails.requestStatus;
              finalObject.approvedAmount=approvalDetails.approvedAmount;
              finalObject.acceptLevel=approvalDetails.acceptLevel+1;
            }else{
              finalObject.acceptLevel=approvalDetails.acceptLevel+1;
            }

          } else if(approvalDetails.requestStatus=="Rejected"){
            finalObject.finalStatus=approvalDetails.requestStatus;
            finalObject.acceptLevel=approvalDetails.acceptLevel+1;
            finalObject.rejectComment=approvalDetails.comment;
          }
          var Sms = server.models.Sms;
          fieldVisitDetails.updateAttributes(finalObject, function(err, finalData){
            if(finalObject.finalStatus!=undefined && finalObject.finalStatus!=null && finalObject.finalStatus=="Rejected" ){
              if(finalData.createdPersonId!=undefined && finalData.createdPersonId!=null && finalData.createdPersonId!='' ){
                var Employee=server.models.Employee;
                var EmailTemplete = server.models.EmailTemplete;
                Employee.find({'where':{'employeeId':finalData.createdPersonId}}, function (err, employeeDetails) {
                  if(employeeDetails!=null && employeeDetails.length>0){

                    EmailTemplete.findOne({"where":{"emailType": "projectWardWorks"}},function (err, emailTemplete) {
                      if(emailTemplete!=null ) {
                        var message = emailTemplete.billGenerationRejectedMessage + '<br>Reason is ' + approvalDetails.comment;
                        var Dhanbademail = server.models.DhanbadEmail;
                        var notification=server.models.notification;
                        notification.create({
                          'to':[{'employeeId':employeeDetails[0].employeeId,'readStatus':false}],
                          "subject":  'New Tasks',
                          "text":"Your project Bill  Request is Rejected",
                          "message":"Your project Bill Request is Rejected",
                          'urlData':'billGeneration',
                          "type":"rejectRequest"
                        }, function (err, notificationDetails){
                          console.log('after update new plan request rejected');
                        });
                        Dhanbademail.create({
                          from: emailTemplete.emailId,
                          to: employeeDetails[0].email,
                          subject: emailTemplete.billGenerationRejectedEmail, // Subject line
                          "text":message
                        }, function (err, email) {
                          console.log(email);
                        });

                        if(emailTemplete.billGenerationRejectedSMS && employeeDetails[0].mobile) {
                          var smsData = {
                            "message": emailTemplete.billGenerationRejectedSMS,
                            "mobileNo": employeeDetails[0].mobile,
                            "smsservicetype": "singlemsg"
                          };

                          Sms.create(smsData, function (err, smsInfo) {
                            console.log('SMS info:' + JSON.stringify(smsInfo));
                          });
                        }
                      }
                    });
                    cb(null, finalData);
                  }else{
                    cb(null, finalData);
                  }
                });

              }else{
                cb(null, finalData);
              }

            }
            else if(finalObject.finalStatus!=undefined && finalObject.finalStatus!=null && finalObject.finalStatus=="Approval"){

              if(finalData.createdPersonId!=undefined && finalData.createdPersonId!=null && finalData.createdPersonId!='' ){
                var Employee=server.models.Employee;
                var EmailTemplete = server.models.EmailTemplete;
                Employee.find({'where':{'employeeId':finalData.createdPersonId}}, function (err, employeeDetails) {
                  if(employeeDetails!=null && employeeDetails.length>0){
                    EmailTemplete.findOne({"where":{"emailType": "projectWardWorks"}},function (err, emailTemplete) {
                      if(emailTemplete!=null) {
                        var message = emailTemplete.billGenerationRequestApprovalMessage + '<br>Reason is ' + approvalDetails.comment;
                        var Dhanbademail = server.models.DhanbadEmail;
                        var notification=server.models.notification;
                        notification.create({
                          'to':[{'employeeId':employeeDetails[0].employeeId,'readStatus':false}],
                          "subject":  'New Tasks',
                          "text":"Your project Bill is approved",
                          "message":"Your project Bill Request is approved",
                          'urlData':'billGeneration',
                          "type":"approvedRequest"
                        }, function (err, notificationDetails){
                          console.log('after update new plan request Approved');
                        });
                        Dhanbademail.create({
                          from: emailTemplete.emailId,
                          to: employeeDetails[0].email,
                          subject: emailTemplete.billGenerationRequestApprovalEmail, // Subject line
                          "text":message
                        }, function (err, email) {
                          console.log(email);
                        });

                        if(emailTemplete.billGenerationRequestApprovalSMS && employeeDetails[0].mobile) {
                          var smsData = {
                            "message": emailTemplete.billGenerationRequestApprovalSMS,
                            "mobileNo": employeeDetails[0].mobile,
                            "smsservicetype": "singlemsg"
                          };

                          Sms.create(smsData, function (err, smsInfo) {
                            console.log('SMS info:' + JSON.stringify(smsInfo));
                          });
                        }
                      }
                    });
                    cb(null, finalData);
                  }else{
                    cb(null, finalData);
                  }
                });
              }
            }
            else{
              if(finalData.workflowId!=undefined && finalData.workflowId!=null && finalData.workflowId!=''){
                var WorkflowEmployees=server.models.WorkflowEmployees;
                var notification=server.models.notification;
                WorkflowEmployees.find({'where':{'workflowId':finalData.workflowId}},function (err, listData) {
                  var workflowData = [];
                  var employeeDetailsForNotification=[];
                  if (listData != undefined && listData != null && listData.length > 0) {
                    for (var i = 0; i < listData.length; i++) {
                      if(parseInt(listData[i].levelNo)==(finalData.acceptLevel+1)){
                        if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                          for(var j=0;j<listData[i].employees.length;j++){
                            employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                          }
                        }
                      }
                    }
                  }
                  if(employeeDetailsForNotification.length>0){
                    notification.create({
                      'to':employeeDetailsForNotification,
                      "subject":  'New Tasks',
                      "text":"You got New Project Bill  Request",
                      "message":"You got New Project Bill Request",
                      'urlData':'approvedRequest',
                      "type":"newRequest"
                    }, function (err, notificationDetails){
                      cb(null, finalData);
                    });

                  }else{
                    cb(null, finalData);
                  }
                });
              }else{
                cb(null, finalData);
              }

            }
          });


        }else{
          var error = new Error('This level is already approved. Can you please refresh page');
          error.statusCode = 200;
          cb(error,null);
        }

      }else {
        var error = new Error('Your plan Field Visit details does not exits');
        error.statusCode = 200;
        cb(error,null);
      }

    });


  };

  Billgeneration.remoteMethod('updateDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateDetails',
      verb: 'POST'
    }
  });

  Billgeneration.updateContent= function (updatedDetails, cb) {

    Billgeneration.findOne({'where':{'id':updatedDetails.id}},function(err, fieldVisitDetails){
      var WorkflowForm=server.models.WorkflowForm;
      var Workflow=server.models.Workflow;
      var WorkflowEmployees=server.models.WorkflowEmployees;
      var employeeDetailsForNotification=[];
      var notification=server.models.notification;
      WorkflowForm.findOne({"where": {"and":[{"status": "Active"},{"schemeUniqueId": "fieldVisit"}]}}, function (err, workflowForm) {
        console.log('work form '+JSON.stringify(workflowForm));
        if(workflowForm!=undefined && workflowForm!=null ){
          Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
            WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
              var workflowData = [];
              if (listData != undefined && listData != null && listData.length > 0) {
                for (var i = 0; i < listData.length; i++) {
                  var flowData = {
                    workflowId: listData[i].workflowId,
                    levelId: listData[i].levelId,
                    status: listData[i].status,
                    maxLevels: listData[i].maxLevels,
                    levelNo: listData[i].levelNo
                  }
                  workflowData.push(flowData);
                  if(listData[i].levelNo=="1"){
                    if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                      for(var j=0;j<listData[i].employees.length;j++){
                        employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                      }
                    }
                  }
                }
              }
              updatedDetails.workflow = workflowData;
              updatedDetails.workflowId=workflowForm.workflowId;
              updatedDetails.maxlevel=flowDataList.maxLevel;
              updatedDetails.acceptLevel=0;
              updatedDetails.finalStatus=false;
              updatedDetails.lastUpdateTime=new Date();
              updatedDetails['updatedTime']=new Date();
              notification.create({
                'to':employeeDetailsForNotification,
                "subject":  'New Tasks',
                "text":"You got edit Bill Request",
                "message":"You got edit Bill Request",
                'urlData':'approvedRequest',
                "type":"newRequest"
              }, function (err, notificationDetails){
              });
              fieldVisitDetails.updateAttributes(updatedDetails,function(err,data){
              });
              cb(null, fieldVisitDetails);
            });
          });
        }
      });
    });
  };

  Billgeneration.remoteMethod('updateContent', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateContent',
      verb: 'POST'
    }
  });


};
