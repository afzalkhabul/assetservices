module.exports = function(Projectdeliverable) {
  Projectdeliverable.observe('before save', function (ctx, next) {

    if (ctx.instance != undefined && ctx.instance != null) {
      ctx.instance.createdTime = new Date();
      next();
    } else {
      ctx.data.updatedTime = new Date();
      next();



    }
  });
};
