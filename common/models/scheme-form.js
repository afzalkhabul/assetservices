module.exports = function(Schemeform) {
	Schemeform.observe('before save', function (ctx, next) {
    if (ctx.instance != undefined && ctx.instance != null) {

    var randomstring = require("randomstring");
     var uniqueId = randomstring.generate({
     length: 15,
     charset: 'alphanumeric'
     });
     ctx.instance.applicationId=uniqueId;
    ctx.instance.createdTime = new Date();
    next();
  } else {
    ctx.data.updatedTime = new Date();
    next();
  }
  });

};
