
var server = require('../../server/server');
module.exports = function(Createcase) {

 Createcase.observe('loaded', function(ctx, next) {
      if(ctx.instance){
        var Advocate = server.models.Advocate;
        var advocateName=ctx.instance.advocate;
        ctx.instance['advocateList']=[];
        if(advocateName!=null && advocateName.length>0){
          var count=0;
          for(var i=0;i<advocateName.length;i++){
            var advocateId=advocateName[i].advId;
            Advocate.find({'where':{'advId':advocateId}}, function (err, advocateList) {
              count++;
              if(advocateList!=null && advocateList.length>0){
                var advocateDetails=advocateList[0]
                ctx.instance.advocateList.push({
                  'name':advocateDetails.name,
                  'email':advocateDetails.email,
                  'advId':advocateDetails.advId
                });
              }
             if(count==advocateName.length){
                next();
             }
            });
          }
        }else{
          next();
        }
      }else{
        next();
      }

    });


  Createcase.observe('before save', function (ctx, next) {
    if(ctx.instance!=undefined && ctx.instance!=null){
      ctx.instance.createdTime = new Date();
      var randomstring = require("randomstring");
      var caseDepartment = ctx.instance.department;
      var caseNumber = randomstring.generate({
        length: 4,
        charset: 'numeric'
      });
      ctx.instance['caseNumber'] = ctx.instance.caseDate + "/"+caseNumber+"/"+caseDepartment.substring(0,3);
      var caseCheckDate = ctx.instance.caseDate;
      var holidayList = server.models.Holiday;
      holidayList.find({"where":{"festivalDate":caseCheckDate}},function (err, Holiday) {
      if(Holiday.length>0){
       var error = new Error("We have holiday for this case submission date");
        error.statusCode = 200;
        next(error);
      }else{
      next()
      }
      });
    }else {
      ctx.data.updatedTime = new Date();
      next();
    }
  });



  Createcase.observe('after save', function (ctx, next) {
    if(ctx.isNewInstance){
    var Emailtemplete = server.models.EmailTemplete;
      Emailtemplete.find({"where":{"emailType":"legal"}},function (err, legalemailtemplate) {
        var Dhanbademail = server.models.DhanbadEmail;
        var Advocate = server.models.Advocate;
        var Sms = server.models.Sms;
        for(var i=0;i<ctx.instance.advocate.length;i++) {
          Dhanbademail.create({
            "to": ctx.instance.advocate[i].email,
            "subject": legalemailtemplate[0].caseSubject,
            "text": "Details :For Case Number : " + ctx.instance.caseNumber + " You Are Assigned"+ legalemailtemplate[0].caseText
          }, function (err, email) {
          });
          if(legalemailtemplate[0].caseSMS) {
            Advocate.find({"where": {"email": ctx.instance.advocate[i].email}}, function (err, advocateData) {
              if (err) {

              } else {
                if (advocateData.length > 0) {
                  if (advocateData[0].phoneNumber) {
                    var smsData = {
                      "message": legalemailtemplate[0].caseSMS,
                      "mobileNo": advocateData[0].phoneNumber,
                      "smsservicetype": "singlemsg"
                    };

                    Sms.create(smsData, function (err, smsInfo) {
                    });
                  }
                }
              }
            });
          }
        }
        next();
      });

    } else {
      next();
    }

  });
};
