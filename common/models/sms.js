module.exports = function(Sms) {
  Sms.beforeCreate = function(next, sms) {
    var utf8 = require('utf8');
    var http = require('http');

    var message = sms.message;
    message = message.toString().replace(/ /g,"%20");


    var mobileNo = sms.mobileNo;
    mobileNo = mobileNo.toString().replace(/,/g,"%2C");

    //http://msdgweb.mgov.gov.in/esms/sendsmsrequest?username=uidjharsms-udd&password=udd#@1&smsservicetype=singlemsg&content=message%22new&mobileno=XXXXXXXXXX&senderid=UDDJHR

    var path;
    if(sms.smsservicetype == 'bulkmsg'){
      path = '/esms/sendsmsrequest?username=uidjharsms-udd&password=udd#@1&smsservicetype=bulkmsg&content='+message+'&bulkmobno='+mobileNo+'&senderid=UDDJHR'
    }else{
      path = '/esms/sendsmsrequest?username=uidjharsms-udd&password=udd#@1&smsservicetype=singlemsg&content='+message+'&mobileno='+mobileNo+'&senderid=UDDJHR'
    }
    var options = {
      host: 'msdgweb.mgov.gov.in',
      path: path
    };

    var req = http.request(options, function(res){
      res.setEncoding('utf8');
      res.on('data', function (chunk) {

      });
    });

    req.end();
    next();
  };
};
